#ifdef magicaVoxel_File_cpp
#error Multiple inclusion
#endif
#define magicaVoxel_File_cpp

#ifndef magicaVoxel_File_hpp
#include "magicaVoxel_File.hpp"
#endif



namespace magicaVoxel
{
  bool File::verifyMagicNumber(u32 magic_number, char *expected_string)
  {
    struct expected
    {
      u32 value;
      u8  null;
    };

    return expected_string[0] != 0 &&
           expected_string[1] != 0 &&
           expected_string[2] != 0 &&
           expected_string[3] != 0 &&
           expected_string[4] == 0 &&
           magic_number == ((expected *)expected_string)->value;
  }

  void File::ChunkYard::initFromFileData(u8 *data, u64 size)
  {
    *this = {0};
    u64 size_read = 0;

    BREAKABLE_START
    {
      if (size < size_read + sizeof(File::Header)) break;
      File::Header *file_header = (File::Header *) &data[size_read];
      if (!File::verifyMagicNumber(file_header->magicNumber, "VOX ") ||
          file_header->version != 150)
      {
        break;
      }
      size_read += sizeof(File::Header);

      u32 parent = 0;

      while (size_read < size - sizeof(File::ChunkHeader))
      {
        File::Chunk *chunk = (File::Chunk *) &data[size_read];
        u64 chunk_size = sizeof(File::ChunkHeader) + chunk->header.contentSize;
        u64 chunk_end  = size_read + chunk_size;

        if (size < chunk_end)
        {
          // skip incomplete chunk at end of file
          break;
        }

        if (parent &&
            refs[parent].childChunksEnd < chunk_end)
        {
          // skip incomplete chunk at end of parent chunk
          size_read = refs[parent].childChunksEnd;
          parent = refs[parent].parent;
          continue;
        }

        u32 index = count++;
        File::ChunkReference *chunk_ref = &refs[index];
        chunk_ref->chunk  = chunk;
        chunk_ref->parent = parent;
        chunk_ref->childChunksEnd = chunk_end + chunk->header.childrenSize;
        size_read = chunk_end;

        if (size_read == refs[parent].childChunksEnd)
        {
          // finished reading the children of the current parent
          if (parent)
          {
            // pop one parent
            parent = refs[parent].parent;
          }
          else
          {
            // no parents to pop, we're done!
            break;
          }
        }
        else if (chunk->header.childrenSize)
        {
          parent = index;
        }
      }
    }
    BREAKABLE_END
  }

  bool File::initUsingFileData(u8 *data, u64 size)
  {
    bool result = false;

    File::Chunk *main_chunk;
    File::Chunk *size_chunk;
    File::Chunk *voxel_chunk;
    File::Chunk *palette_chunk;

    BREAKABLE_START
    {
      bool error = false;

      {
        File::ChunkYard yard;
        yard.initFromFileData(data, size);

        main_chunk    = yard.refs[0].chunk;
        size_chunk    = 0;
        voxel_chunk   = 0;
        palette_chunk = 0;

        for (u32 i = 1; i < yard.count; ++i)
        {
          if (File::verifyMagicNumber(yard.refs[i].chunk->header.magicNumber,
                                                 "SIZE"))
          {
            if (size_chunk)
            {
              error = true;
              continue;
            }

            size_chunk = yard.refs[i].chunk;

            if (size_chunk->header.contentSize != memberSize(File::ChunkContent, size) ||
                size_chunk->content.size.x >= 255 ||
                size_chunk->content.size.y >= 255 ||
                size_chunk->content.size.z >= 255)
            {
              error = true;
            }

            continue;
          }

          if (File::verifyMagicNumber(yard.refs[i].chunk->header.magicNumber,
                                                 "XYZI"))
          {
            if (voxel_chunk)
            {
              error = true;
              continue;
            }

            voxel_chunk = yard.refs[i].chunk;

            if (voxel_chunk->header.contentSize !=
                  offsetOf(memberType(File::ChunkContent, voxel), voxels) +
                  (voxel_chunk->content.voxel.count * sizeof(File::Voxel)))
            {
              error = true;
            }

            continue;
          }

          if (File::verifyMagicNumber(yard.refs[i].chunk->header.magicNumber,
                                                 "RGBA"))
          {
            if (palette_chunk)
            {
              error = true;
              continue;
            }

            palette_chunk = yard.refs[i].chunk;

            if (palette_chunk->header.contentSize !=
                  memberSize(File::ChunkContent, palette))
            {
              error = true;
            }

            continue;
          }
        }

        // NB. Palette chunk is optional
        if (error || !size_chunk || !voxel_chunk) break;
      }

      *this = {0};
      palette    = palette_chunk ?
                         &palette_chunk->content.palette :
                         &Palette::DEFAULT;
      voxels     = voxel_chunk->content.voxel.voxels;
      voxelCount = voxel_chunk->content.voxel.count;
      gridSize   = v3<u8>(size_chunk->content.size);

      result = true;
    }
    BREAKABLE_END

    return result;
  }
}
