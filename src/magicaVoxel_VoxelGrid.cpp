#ifdef magicaVoxel_VoxelGrid_cpp
#error Multiple inclusion
#endif
#define magicaVoxel_VoxelGrid_cpp

#ifndef magicaVoxel_VoxelGrid_hpp
#include "magicaVoxel_VoxelGrid.hpp"
#endif



namespace magicaVoxel
{
  size_t VoxelGrid::sizeWith(V3<u8> size)
  {
    return offsetOf(VoxelGrid,
                    voxels[((size_t)size.x) *
                           ((size_t)size.y) *
                           ((size_t)size.z)]);
  }

  u8 *VoxelGrid::getVoxelPtr(V3<i16> pos)
  {
    u8 *result = 0;

    if (0 <= pos.x && pos.x < size.x &&
        0 <= pos.y && pos.y < size.y &&
        0 <= pos.z && pos.z < size.z)
    {
      result = &voxels[(((pos.x * size.y) + pos.y) * size.z) + pos.z];
    }

    return result;
  }

  u8 VoxelGrid::getVoxel(V3<i16> pos)
  {
    u8 result = 0;

    u8 *ptr = this->getVoxelPtr(pos);

    if (ptr) result = *ptr;

    return result;
  }
}
