#ifdef magicaVoxel_loadMesh_cpp
#error Multiple inclusion
#endif
#define magicaVoxel_loadMesh_cpp

#ifndef magicaVoxel_loadMesh_hpp
#include "magicaVoxel_loadMesh.hpp"
#endif

#ifndef ScopeExit_hpp
#error "Please include ScopeExit.hpp before this file"
#endif

#ifndef List_hpp
#error "Please include List.hpp before this file"
#endif

#ifndef Grid_hpp
#error "Please include Grid.hpp before this file"
#endif

#ifndef Hash_hpp
#error "Please include Hash.hpp before this file"
#endif

#ifndef map_hpp
#error "Please include map.hpp before this file"
#endif


namespace magicaVoxel
{
  namespace loadMesh
  {
    namespace Direction3d
    {
      enum Enum
      {
        X_POSITIVE,
        X_NEGATIVE,
        Y_POSITIVE,
        Y_NEGATIVE,
        Z_POSITIVE,
        Z_NEGATIVE,
        ENUM_COUNT
      };

      char *NAME[ENUM_COUNT] =
      {
        "X_POSITIVE",
        "X_NEGATIVE",
        "Y_POSITIVE",
        "Y_NEGATIVE",
        "Z_POSITIVE",
        "Z_NEGATIVE",
      };

      DEFINE_ENUM_PREINCREMENT

      u8 COMPONENT_IDX[ENUM_COUNT] =
      {
        0, 0,
        1, 1,
        2, 2
      };

      Enum INVERT[ENUM_COUNT] =
      {
        X_NEGATIVE,
        X_POSITIVE,
        Y_NEGATIVE,
        Y_POSITIVE,
        Z_NEGATIVE,
        Z_POSITIVE,
      };

      Enum RH_SECONDARY_AXES[ENUM_COUNT][2] =
      {
        { Y_POSITIVE, Z_POSITIVE },
        { Z_NEGATIVE, Y_NEGATIVE },
        { Z_POSITIVE, X_POSITIVE },
        { X_NEGATIVE, Z_NEGATIVE },
        { X_POSITIVE, Y_POSITIVE },
        { Y_NEGATIVE, X_NEGATIVE }
      };

      bool IS_POSITIVE[ENUM_COUNT] =
      {
        true, false,
        true, false,
        true, false
      };

      V3<i16> VECTOR[ENUM_COUNT] =
      {
        v3<i16>( 1,  0,  0),
        v3<i16>(-1,  0,  0),
        v3<i16>( 0,  1,  0),
        v3<i16>( 0, -1,  0),
        v3<i16>( 0,  0,  1),
        v3<i16>( 0,  0, -1)
      };

      V3<u8> CCW_FACE_QUADS[ENUM_COUNT][4] =
      {
        {
          v3<u8>(1, 0, 0),
          v3<u8>(1, 1, 0),
          v3<u8>(1, 1, 1),
          v3<u8>(1, 0, 1)
        },
        {
          v3<u8>(0, 0, 0),
          v3<u8>(0, 0, 1),
          v3<u8>(0, 1, 1),
          v3<u8>(0, 1, 0)
        },
        {
          v3<u8>(0, 1, 0),
          v3<u8>(0, 1, 1),
          v3<u8>(1, 1, 1),
          v3<u8>(1, 1, 0)
        },
        {
          v3<u8>(0, 0, 0),
          v3<u8>(1, 0, 0),
          v3<u8>(1, 0, 1),
          v3<u8>(0, 0, 1)
        },
        {
          v3<u8>(0, 0, 1),
          v3<u8>(1, 0, 1),
          v3<u8>(1, 1, 1),
          v3<u8>(0, 1, 1)
        },
        {
          v3<u8>(0, 0, 0),
          v3<u8>(0, 1, 0),
          v3<u8>(1, 1, 0),
          v3<u8>(1, 0, 0)
        }
      };
    }

    namespace Direction2d
    {
      enum Enum
      {
        X_POSITIVE,
        Y_POSITIVE,
        X_NEGATIVE,
        Y_NEGATIVE,
        ENUM_COUNT
      };

      u8 COMPONENT_IDX[ENUM_COUNT] =
      {
        0, 1,
        0, 1
      };

      Enum INVERT[ENUM_COUNT] =
      {
        X_NEGATIVE,
        Y_NEGATIVE,
        X_POSITIVE,
        Y_POSITIVE,
      };

      bool IS_POSITIVE[ENUM_COUNT] =
      {
        true, true,
        false, false
      };

      V2<i16> VECTOR[ENUM_COUNT] =
      {
        v2<i16>( 1,  0),
        v2<i16>( 0,  1),
        v2<i16>(-1,  0),
        v2<i16>( 0, -1)
      };

      // Rotate 90 degrees CCW, n times
      Enum rotate(Enum d, i32 n)
      {
        assert(X_POSITIVE <= d && d < ENUM_COUNT);

        i32 count = (i32) ENUM_COUNT;
        i32 result = n + (i32)d;
        result %= count;
        result += count;
        result %= count;

        return (Enum) result;
      }

      // size of turn, in 90 degrees CCW units
      // result given in range -1 to 2, ie:
      //   -1 =  90 degrees CW
      //    0 =  no turn
      //    1 =  90 degrees CCW
      //    2 = 180 degrees
      i32 measureTurn(Enum in, Enum out)
      {
        i32 result = ((i32)out) - ((i32)in);

        i32 count = (i32) ENUM_COUNT;
        result %= count;

        if (result < -1)     result += count;
        else if (2 < result) result -= count;

        return (Enum) result;
      }

      // WARNING: If v is *not* parallel to one of the four cardinal
      // directions, this will return ENUM_COUNT
      template <typename T>
      Enum fromVector(V2<T> v)
      {
        Enum result;

        if (v.x != 0)
        {
          if (v.y != 0)
          {
            // Not a cardinal direction
            result = ENUM_COUNT;
          }
          else
          {
            result = 0 < v.x ? X_POSITIVE : X_NEGATIVE;
          }
        }
        else
        {
          if (v.y != 0)
          {
            result = 0 < v.y ? Y_POSITIVE : Y_NEGATIVE;
          }
          else
          {
            // v is zero
            result = ENUM_COUNT;
          }
        }

        return result;
      }
    }

    namespace Quadrant
    {
      // Numbered anticlockwise from the x-axis:
      //
      //    +y
      //   2 | 1
      //  ---+--- +x
      //   3 | 4
      //
      enum Enum
      {
        FIRST,
        SECOND,
        THIRD,
        FOURTH,
        ENUM_COUNT
      };

      char *NAME[ENUM_COUNT] =
      {
        "FIRST",
        "SECOND",
        "THIRD",
        "FOURTH",
      };

      DEFINE_ENUM_PREINCREMENT

      // Rotate 90 degrees CCW, n times
      Enum rotate(Enum q, i32 n)
      {
        assert(FIRST <= q && q < ENUM_COUNT);

        i32 count = (i32) ENUM_COUNT;
        i32 result = n + (i32)q;
        result %= count;
        result += count;
        result %= count;

        return (Enum) result;
      }

      Direction2d::Enum DIRECTION_BEFORE[4] =
      {
        Direction2d::X_POSITIVE,
        Direction2d::Y_POSITIVE,
        Direction2d::X_NEGATIVE,
        Direction2d::Y_NEGATIVE
      };

      Direction2d::Enum DIRECTION_AFTER[4] =
      {
        Direction2d::Y_POSITIVE,
        Direction2d::X_NEGATIVE,
        Direction2d::Y_NEGATIVE,
        Direction2d::X_POSITIVE
      };

      // WARNING: If v is parallel to one of the four cardinal directions,
      // this will return ENUM_COUNT
      template <typename T>
      Enum fromVector(V2<T> v)
      {
        Enum result;

        if (v.x == 0 || v.y == 0) result = ENUM_COUNT;
        else if (0 < v.x)         result = v.y < 0 ? SECOND : FIRST;
        else                      result = v.y < 0 ?  THIRD : FOURTH;

        return result;
      }

      typedef u32 Bitfield;
      Bitfield BIT[4] = { 0b0001, 0b0010, 0b0100, 0b1000 };
      Bitfield NO_BITS = 0b0000;
      Bitfield ALL_BITS = 0b1111;
    }

    V2<i16> voxelOffset2d(Direction3d::Enum facing_dir,
                          Quadrant::Enum quadrant)
    {
      static V2<i16> offset[Quadrant::ENUM_COUNT] =
      {
        v2<i16>(0,0),   v2<i16>(-1,0),
        v2<i16>(-1,-1), v2<i16>(0,-1)
      };

      V2<i16> result = offset[quadrant];
      // For a negative surface, a vertex is tied to the voxel in the third quadrant
      if (!Direction3d::IS_POSITIVE[facing_dir]) result += v2<i16>(1);
      return result;
    }

    V3<i16> voxelOffset3d(Direction3d::Enum facing_dir,
                          Quadrant::Enum quadrant)
    {
      // If we're looking at a positive face at this vertex,
      // it's actually on the voxel behind us
      return v3<i16>(Direction3d::IS_POSITIVE[facing_dir] ? -1 : 0,
                     voxelOffset2d(facing_dir, quadrant));
    }

    namespace Direction2d
    {
      Quadrant::Enum QUADRANT_BEFORE[4] =
      {
        Quadrant::FOURTH,
        Quadrant::FIRST,
        Quadrant::SECOND,
        Quadrant::THIRD
      };

      Quadrant::Enum QUADRANT_AFTER[4] =
      {
        Quadrant::FIRST,
        Quadrant::SECOND,
        Quadrant::THIRD,
        Quadrant::FOURTH
      };
    }

    V3<i16> vectorToRHBasis(V3<i16> v, Direction3d::Enum bx)
    {
      Direction3d::Enum
        by = Direction3d::RH_SECONDARY_AXES[bx][0],
        bz = Direction3d::RH_SECONDARY_AXES[bx][1];
      return (v.x * Direction3d::VECTOR[bx]) +
             (v.y * Direction3d::VECTOR[by]) +
             (v.z * Direction3d::VECTOR[bz]);
    }

    V3<i16> vectorFromRHBasis(V3<i16> v, Direction3d::Enum bx)
    {
      Direction3d::Enum
        by = Direction3d::RH_SECONDARY_AXES[bx][0],
        bz = Direction3d::RH_SECONDARY_AXES[bx][1];

      V3<i16> result;

      result.x = v.v[Direction3d::COMPONENT_IDX[bx]] * (Direction3d::IS_POSITIVE[bx] ? 1 : -1);
      result.y = v.v[Direction3d::COMPONENT_IDX[by]] * (Direction3d::IS_POSITIVE[by] ? 1 : -1);
      result.z = v.v[Direction3d::COMPONENT_IDX[bz]] * (Direction3d::IS_POSITIVE[bz] ? 1 : -1);

      return result;
    }

    struct TmpVertex
    {
      V2<u8> pos;
      u8 color;

      bool operator==(const TmpVertex &that) const
      {
        return (this->pos   == that.pos) &&
               (this->color == that.color);
      }

      static u32 idealIdx(u8 hash_size, TmpVertex vertex)
      {
        return Hash32::SEED().hash(vertex.pos.x)
                             .hash(vertex.pos.y)
                             .hash(vertex.color)
                             .fold(hash_size);
      }
    };

    struct IntermediateVertex
    {
      V3<u8> pos;
      Direction3d::Enum normal;
      u8 colour;
    };

    bool operator==(const IntermediateVertex &vertex,
                    const IntermediateVertex &other)
    {
      return (vertex.pos.x  == other.pos.x)  &&
             (vertex.pos.y  == other.pos.y)  &&
             (vertex.pos.z  == other.pos.z)  &&
             (vertex.normal == other.normal) &&
             (vertex.colour == other.colour);
    }

    u64 IntermediateVertex_hash(IntermediateVertex *vertex)
    {
      u64 prime  = 31;
      u64 result = 131;
      result = (prime * result) + vertex->pos.x;
      result = (prime * result) + vertex->pos.y;
      result = (prime * result) + vertex->pos.z;
      result = (prime * result) + vertex->normal;
      result = (prime * result) + vertex->colour;
      return result;
    }

    struct IntermediateVertexHashTableBucket
    {
      IntermediateVertexHashTableBucket *next;
      IntermediateVertex vertex;
    };

    u8 getQuadrantColor(VoxelGrid *grid,
                        V3<i16> vertex,
                        Direction3d::Enum facing_dir,
                        Quadrant::Enum quadrant)
    {
      u8 result;

      V3<i16> voxel_pos = vertex + vectorToRHBasis(voxelOffset3d(facing_dir, quadrant), facing_dir);
      V3<i16> opposite_pos = voxel_pos + Direction3d::VECTOR[facing_dir];

      result = grid->getVoxel(opposite_pos) ? 0
             : grid->getVoxel(voxel_pos);

      return result;
    }

    struct Polygon
    {
      List<V2<i16>> vertices;
      u8            color;

      NO_COPY_OR_MOVE(Polygon)

      void init(u8 color)
      {
        vertices.init();
        this->color = color;
      }
    };

    struct PolygonHoles
    {
      Polygon       *polygon;
      List<Polygon>  holes;

      NO_COPY_OR_MOVE(PolygonHoles)

      void init(Polygon *polygon)
      {
        this->polygon = polygon;
        holes.init();
      }
    };

    Mesh *from(HeapAllocator *heap_alloc, VoxelGrid *grid, Palette *palette)
    {
      PageAllocator tmp_alloc;
      tmp_alloc.init(heap_alloc);
      SCOPE_EXIT(tmp_alloc.destroy());

      return from(heap_alloc, tmp_alloc, grid, palette);
    }

    Mesh *from(HeapAllocator *heap_alloc, PageAllocator tmp_alloc, VoxelGrid *grid, Palette *palette)
    {
      Mesh::Vertex *tmp_v_buffer;
      {
        // FIXME - less naive worst case?
        u32 max_vertices = 24 * grid->voxelCount;
        tmp_v_buffer = allocArray<Mesh::Vertex>(&tmp_alloc, max_vertices);
      }

      u32 v_buffer_idx = 0;

      u32 *tmp_i_buffer;
      {
        // FIXME - less naive worst case?
        u32 max_faces = 6 * grid->voxelCount;
        tmp_i_buffer = allocArray<u32>(&tmp_alloc, max_faces * 6);
      }

      u32 i_buffer_idx = 0;

      for (Direction3d::Enum a_axis = Direction3d::X_POSITIVE;
           a_axis < Direction3d::ENUM_COUNT; ++a_axis)
      {
        PageAllocator per_dir_alloc = tmp_alloc;
        List<V2<i16>> free_vertex_links;
        List<Polygon> free_polygon_links;
        List<PolygonHoles> free_polygon_holes_links;

        Direction3d::Enum
          b_axis = Direction3d::RH_SECONDARY_AXES[a_axis][0],
          c_axis = Direction3d::RH_SECONDARY_AXES[a_axis][1];

        u8 a_max = grid->size.v[Direction3d::COMPONENT_IDX[a_axis]];
        u8 b_max = grid->size.v[Direction3d::COMPONENT_IDX[b_axis]];
        u8 c_max = grid->size.v[Direction3d::COMPONENT_IDX[c_axis]];

        V3<i16> base_corner = Direction3d::IS_POSITIVE[a_axis]
                            ? v3<i16>(0)
                            : v3<i16>(grid->size);

        typedef map::Variable<TmpVertex, u32, TmpVertex::idealIdx> VertexMap;
        VertexMap *vertex_map;
        {
          u32 max_vertices = 4 * b_max * c_max;
          u32 map_size = map::chooseSize(max_vertices);
          vertex_map = allocUninitSized<VertexMap>(&per_dir_alloc,
                           VertexMap::sizeWith(map_size));
          vertex_map->init(map_size);
          vertex_map->clear();
        }

        // +1 to both dimensions since the vertices are fenceposts
        pGrid<Quadrant::Bitfield> eliminated_quadrants =
          Grid<Quadrant::Bitfield>::allocUninit(&per_dir_alloc, b_max+2, c_max+2);

        // NB. We'll never get a face at 0 in the facing direction - it would
        // be on the outside boundary facing in. We're also iterating vertices
        // here, not faces, so we want to cover 1 through max *INCLUSIVE*
        for (i16 a = 1; a <= a_max; ++a)
        {
          eliminated_quadrants.zero();
          List<Polygon> polygons;
          List<Polygon> unidentified_holes;

          // NB. Still vertices, so 0 to max *INCLUSIVE*
          for (i16 b = 0; b <= b_max; ++b)
          {
            for (i16 c = 0; c <= c_max; ++c)
            {
              Quadrant::Bitfield *start_vertex_eliminated_quadrants = &eliminated_quadrants[b][c];

              *start_vertex_eliminated_quadrants |=
                ((b == b_max || c == c_max) ? Quadrant::BIT[Quadrant::FIRST]  : 0) |
                ((b == 0     || c == c_max) ? Quadrant::BIT[Quadrant::SECOND] : 0) |
                ((b == 0     || c == 0    ) ? Quadrant::BIT[Quadrant::THIRD]  : 0) |
                ((b == b_max || c == 0    ) ? Quadrant::BIT[Quadrant::FOURTH] : 0);

              if (*start_vertex_eliminated_quadrants == Quadrant::ALL_BITS) continue;

              V2<i16> start_vertex = v2(b, c);
              V3<i16> start_vertex_pos = base_corner + vectorToRHBasis(v3(a, start_vertex), a_axis);

              u8 start_quadrant_colors[4];
              for (Quadrant::Enum q = Quadrant::FIRST;
                   q < Quadrant::ENUM_COUNT;
                   ++q)
              {
                u8 color = getQuadrantColor(grid, start_vertex_pos, a_axis, q);

                if (!color) *start_vertex_eliminated_quadrants |= Quadrant::BIT[q];

                start_quadrant_colors[q] = color;
              }

              if ((start_quadrant_colors[0] == start_quadrant_colors[1]) &&
                  (start_quadrant_colors[0] == start_quadrant_colors[2]) &&
                  (start_quadrant_colors[0] == start_quadrant_colors[3]))
              {
                continue;
              }

              for (Quadrant::Enum start_quadrant = Quadrant::FIRST;
                   start_quadrant < Quadrant::ENUM_COUNT;
                   ++start_quadrant)
              {
                if (*start_vertex_eliminated_quadrants & Quadrant::BIT[start_quadrant]) continue;
                *start_vertex_eliminated_quadrants |= Quadrant::BIT[start_quadrant];

                auto *polygon_link = free_polygon_links.getRecycledLink(&per_dir_alloc);

                Polygon *polygon = &polygon_link->data;
                polygon->init(start_quadrant_colors[start_quadrant]);

                Direction2d::Enum start_in_dir;
                u32 num_quadrants;
                {
                  Direction2d::Enum direction_of_previous;

                  if (start_quadrant_colors[Quadrant::rotate(start_quadrant, 1)] != polygon->color)
                  {
                    direction_of_previous = Quadrant::DIRECTION_AFTER[start_quadrant];
                    num_quadrants = 1;
                  }
                  else
                  {
                    *start_vertex_eliminated_quadrants |= Quadrant::BIT[Quadrant::rotate(start_quadrant, 1)];

                    if (start_quadrant_colors[Quadrant::rotate(start_quadrant, 2)] != polygon->color)
                    {
                      direction_of_previous = Quadrant::DIRECTION_AFTER[Quadrant::rotate(start_quadrant, 1)];
                      num_quadrants = 2;
                    }
                    else
                    {
                      *start_vertex_eliminated_quadrants |= Quadrant::BIT[Quadrant::rotate(start_quadrant, 2)];

                      assert(start_quadrant_colors[Quadrant::rotate(start_quadrant, 3)] != polygon->color);
                      direction_of_previous = Quadrant::DIRECTION_AFTER[Quadrant::rotate(start_quadrant, 2)];
                      num_quadrants = 3;
                    }
                  }

                  start_in_dir = Direction2d::INVERT[direction_of_previous];
                }

                Direction2d::Enum start_out_dir;

                if (start_quadrant_colors[Quadrant::rotate(start_quadrant, -1)] != polygon->color)
                {
                  start_out_dir = Quadrant::DIRECTION_BEFORE[start_quadrant];
                }
                else
                {
                  *start_vertex_eliminated_quadrants |= Quadrant::BIT[Quadrant::rotate(start_quadrant, -1)];

                  if (start_quadrant_colors[Quadrant::rotate(start_quadrant, -2)] != polygon->color)
                  {
                    start_out_dir = Quadrant::DIRECTION_BEFORE[Quadrant::rotate(start_quadrant, -1)];
                  }
                  else
                  {
                    *start_vertex_eliminated_quadrants |= Quadrant::BIT[Quadrant::rotate(start_quadrant, -2)];

                    assert(start_quadrant_colors[Quadrant::rotate(start_quadrant, -3)] != polygon->color);
                    start_out_dir = Quadrant::DIRECTION_BEFORE[Quadrant::rotate(start_quadrant, -2)];
                  }
                }

                V2<i16> current_vertex = start_vertex;
                Direction2d::Enum
                  current_in_dir  = start_in_dir,
                  current_out_dir = start_out_dir;
                V3<i16> current_vertex_pos = start_vertex_pos;

                u8 current_quadrant_colors[4] =
                {
                  start_quadrant_colors[0],
                  start_quadrant_colors[1],
                  start_quadrant_colors[2],
                  start_quadrant_colors[3]
                };

                i32 turning_number = Direction2d::measureTurn(current_in_dir, current_out_dir);
                assert(turning_number != 2);

                while (true)
                {
                  bool skip_vertex = false;

                  BREAKABLE_START
                  {
                    // if this vertex is a corner in the polygon, we obviously need it
                    if (current_in_dir != current_out_dir) break;

                    // otherwise this vertex is along a straight edge of the
                    // polygon (and we know that the voxels opposite our "in" and
                    // "out" quadrants are open)

                    Quadrant::Enum
                      other_quadrant_1 = Direction2d::QUADRANT_BEFORE[current_out_dir],
                      other_quadrant_2 = Quadrant::rotate(other_quadrant_1, -1);

                    // we *might* be able to discard it, but if any other polygon
                    // has a corner at this vertex, that might cause a gap between
                    // those polygons due to rounding errors

                    // if the adjacent two quadrants are different colours,
                    // then at least one of them has a corner here
                    if (current_quadrant_colors[other_quadrant_1] != current_quadrant_colors[other_quadrant_2]) break;

                    // since we know that the voxels opposite our polygon are open,
                    // check for a discontinuity in the (possible) edge catercorner to our edge
                    V3<i16> other_opposite_1 = current_vertex_pos +
                      vectorToRHBasis(v3<i16>(1, 0, 0) + voxelOffset3d(a_axis, other_quadrant_1), a_axis);
                    V3<i16> other_opposite_2 = current_vertex_pos +
                      vectorToRHBasis(v3<i16>(1, 0, 0) + voxelOffset3d(a_axis, other_quadrant_2), a_axis);
                    if (grid->getVoxel(other_opposite_1) !=
                        grid->getVoxel(other_opposite_2)) break;

                    skip_vertex = true;
                  }
                  BREAKABLE_END

                  if (!skip_vertex)
                  {
                    List<V2<i16>>::Link *link = free_vertex_links.getRecycledLink(&per_dir_alloc);
                    link->data = current_vertex;
                    polygon->vertices.append(link);
                  }

                  current_vertex += Direction2d::VECTOR[current_out_dir];
                  assert(0 <= current_vertex.x && current_vertex.x <= b_max &&
                         0 <= current_vertex.y && current_vertex.y <= c_max);

                  if (current_vertex == start_vertex)
                  {
                    assert(current_out_dir == start_in_dir);
                    break;
                  }

                  current_vertex_pos = base_corner + vectorToRHBasis(v3(a, current_vertex), a_axis);

                  switch (current_out_dir)
                  {
                    case Direction2d::X_POSITIVE:
                    {
                      current_quadrant_colors[Quadrant::SECOND] =
                        current_quadrant_colors[Quadrant::FIRST];
                      current_quadrant_colors[Quadrant::THIRD] =
                        current_quadrant_colors[Quadrant::FOURTH];

                      current_quadrant_colors[Quadrant::FIRST] =
                        getQuadrantColor(grid, current_vertex_pos, a_axis,
                                                          Quadrant::FIRST);
                      current_quadrant_colors[Quadrant::FOURTH] =
                        getQuadrantColor(grid, current_vertex_pos, a_axis,
                                                          Quadrant::FOURTH);
                    } break;

                    case Direction2d::Y_POSITIVE:
                    {
                      current_quadrant_colors[Quadrant::FOURTH] =
                        current_quadrant_colors[Quadrant::FIRST];
                      current_quadrant_colors[Quadrant::THIRD] =
                        current_quadrant_colors[Quadrant::SECOND];

                      current_quadrant_colors[Quadrant::FIRST] =
                        getQuadrantColor(grid, current_vertex_pos, a_axis,
                                                          Quadrant::FIRST);
                      current_quadrant_colors[Quadrant::SECOND] =
                        getQuadrantColor(grid, current_vertex_pos, a_axis,
                                                          Quadrant::SECOND);
                    } break;

                    case Direction2d::X_NEGATIVE:
                    {
                      current_quadrant_colors[Quadrant::FIRST] =
                        current_quadrant_colors[Quadrant::SECOND];
                      current_quadrant_colors[Quadrant::FOURTH] =
                        current_quadrant_colors[Quadrant::THIRD];

                      current_quadrant_colors[Quadrant::SECOND] =
                        getQuadrantColor(grid, current_vertex_pos, a_axis,
                                                          Quadrant::SECOND);
                      current_quadrant_colors[Quadrant::THIRD] =
                        getQuadrantColor(grid, current_vertex_pos, a_axis,
                                                          Quadrant::THIRD);
                    } break;

                    case Direction2d::Y_NEGATIVE:
                    {
                      current_quadrant_colors[Quadrant::FIRST] =
                        current_quadrant_colors[Quadrant::FOURTH];
                      current_quadrant_colors[Quadrant::SECOND] =
                        current_quadrant_colors[Quadrant::THIRD];

                      current_quadrant_colors[Quadrant::THIRD] =
                        getQuadrantColor(grid, current_vertex_pos, a_axis,
                                                          Quadrant::THIRD);
                      current_quadrant_colors[Quadrant::FOURTH] =
                        getQuadrantColor(grid, current_vertex_pos, a_axis,
                                                          Quadrant::FOURTH);
                    } break;

                    default: assert(false);
                  }

                  assert((!current_quadrant_colors[Quadrant::FIRST])  || (current_vertex.x <= b_max && current_vertex.y <= c_max));
                  assert((!current_quadrant_colors[Quadrant::SECOND]) ||      (0 < current_vertex.x && current_vertex.y <= c_max));
                  assert((!current_quadrant_colors[Quadrant::THIRD])  ||      (0 < current_vertex.x && 0 < current_vertex.y));
                  assert((!current_quadrant_colors[Quadrant::FOURTH]) || (current_vertex.x <= b_max && 0 < current_vertex.y));

                  current_in_dir = current_out_dir;
                  Direction2d::Enum direction_of_previous = Direction2d::INVERT[current_in_dir];
                  current_out_dir = Direction2d::rotate(direction_of_previous, -1);

                  if (current_quadrant_colors[Direction2d::QUADRANT_BEFORE[current_out_dir]] == polygon->color)
                  {
                    current_out_dir = Direction2d::rotate(current_out_dir, -1);

                    if (current_quadrant_colors[Direction2d::QUADRANT_BEFORE[current_out_dir]] == polygon->color)
                    {
                      current_out_dir = Direction2d::rotate(current_out_dir, -1);

                      assert(current_quadrant_colors[Direction2d::QUADRANT_BEFORE[current_out_dir]] != polygon->color);
                    }
                  }

                  Quadrant::Bitfield *current_vertex_eliminated_quadrants = &eliminated_quadrants[current_vertex];

                  for (Quadrant::Enum i = Direction2d::QUADRANT_AFTER[current_out_dir];
                       i != Direction2d::QUADRANT_AFTER[direction_of_previous];
                       i = Quadrant::rotate(i, 1))
                  {
                    *current_vertex_eliminated_quadrants |= Quadrant::BIT[i];
                  }

                  i32 turn = Direction2d::measureTurn(current_in_dir, current_out_dir);
                  assert(turn != 2);
                  turning_number += turn;
                }

                if (turning_number == 4)
                {
                  polygons.append(polygon_link);
                }
                else if (turning_number == -4)
                {
                  unidentified_holes.append(polygon_link);
                }
                else
                {
                  assert(false);
                }
              }
            }
          }

          List<PolygonHoles> polygon_holes;

          while (!unidentified_holes.isEmpty())
          {
            auto *hole_link = unidentified_holes.popFirst();
            Polygon *hole = &hole_link->data;

            // one square of the hole (doesn't matter which)
            V2<i16> hole_position;
            {
              assert(!hole->vertices.isEmpty());

              List<V2<i16>>::Link *first_link = hole->vertices.first();
              List<V2<i16>>::Link *second_link = first_link->next;
              assert(second_link != hole->vertices.sentinel());

              V2<i16> first = first_link->data;
              V2<i16> second = second_link->data;

              // fromVector returns ENUM_COUNT for zero or diagonal vectors
              Direction2d::Enum to_second =
                Direction2d::fromVector(second - first);
              assert(to_second != Direction2d::ENUM_COUNT);

              Quadrant::Enum quadrant_inside_hole =
                Direction2d::QUADRANT_BEFORE[to_second];

              hole_position = first + voxelOffset2d(a_axis, quadrant_inside_hole);
            }

            // grid starts at (0,0)
            i16 nearest_polygon_edge_y = -1;
            Polygon *nearest_polygon = 0;

            for (auto *polygon_link = polygons.first();
                 polygon_link != polygons.sentinel();
                 polygon_link = polygon_link->next)
            {
              Polygon *polygon = &polygon_link->data;

              if (polygon->color != hole->color) continue;

              // grid starts at (0,0)
              i16 nearest_edge_y = -1;
              Direction2d::Enum nearest_edge_direction = Direction2d::ENUM_COUNT;

              V2<i16> current = polygon->vertices.last()->data;

              for (List<V2<i16>>::Link *vertex_link = polygon->vertices.first();
                   vertex_link != polygon->vertices.sentinel();
                   vertex_link = vertex_link->next)
              {
                V2<i16> prev = current;
                current = vertex_link->data;

                if (prev.y != current.y)
                {
                  assert(prev.x == current.x);
                  continue;
                }

                i16 edge_y = current.y;
                if (edge_y <= nearest_edge_y ||
                    hole_position.y <= edge_y)
                {
                  continue;
                }

                Direction2d::Enum edge_direction;

                if (prev.x <= hole_position.x)
                {
                  if (current.x <= hole_position.x)
                  {
                    continue;
                  }
                  else // if (hole_position.x < current.x)
                  {
                    edge_direction = Direction2d::X_POSITIVE;
                  }
                }
                else // if (hole_position.x < prev.x)
                {
                  if (current.x <= hole_position.x)
                  {
                    edge_direction = Direction2d::X_NEGATIVE;
                  }
                  else // if (hole_position.x < current.x)
                  {
                    continue;
                  }
                }

                nearest_edge_y = edge_y;
                nearest_edge_direction = edge_direction;
              }

              if (nearest_edge_direction == Direction2d::X_POSITIVE &&
                  nearest_polygon_edge_y < nearest_edge_y)
              {
                nearest_polygon = polygon;
                nearest_polygon_edge_y = nearest_edge_y;
              }
            }

            assert(nearest_polygon);

            PolygonHoles *nearest_polygon_holes = 0;

            for (auto *polygon_holes_link = polygon_holes.first();
                 polygon_holes_link != polygon_holes.sentinel();
                 polygon_holes_link = polygon_holes_link->next)
            {
              if (polygon_holes_link->data.polygon == nearest_polygon)
              {
                nearest_polygon_holes = &polygon_holes_link->data;
                break;
              }
            }

            if (!nearest_polygon_holes)
            {
              auto *polygon_holes_link =
                free_polygon_holes_links.getRecycledLink(&per_dir_alloc);
              polygon_holes_link->data.init(nearest_polygon);
              polygon_holes.append(polygon_holes_link);
              nearest_polygon_holes = &polygon_holes_link->data;
            }

            nearest_polygon_holes->holes.append(hole_link);
          }

          // We have now emptied the list "holes", and transferred all of the
          // holes into the appropriate sub-list of "polygon_holes".

          while (!polygon_holes.isEmpty())
          {
            auto *polygon_holes_link = polygon_holes.popFirst();
            Polygon *polygon = polygon_holes_link->data.polygon;
            List<Polygon> *holes = &polygon_holes_link->data.holes;

            if (!holes->isEmpty())
            {
              for (auto *vertex_link = polygon->vertices.first();
                   vertex_link != polygon->vertices.sentinel();
                   vertex_link = vertex_link->next)
              {
                V2<i16> vertex = vertex_link->data;
              }

              for (auto *hole_link = holes->first();
                   hole_link != holes->sentinel();
                   hole_link = hole_link->next)
              {
                Polygon *hole = &hole_link->data;

                for (auto *vertex_link = hole->vertices.first();
                     vertex_link != hole->vertices.sentinel();
                     vertex_link = vertex_link->next)
                {
                  V2<i16> vertex = vertex_link->data;
                }
              }
            }

            while (!holes->isEmpty())
            {
              auto *hole_link = holes->first();
              Polygon *hole = &hole_link->data;

              i32 shortest_bridge_length_squared = INT32_MAX;
              List<V2<i16>>::Link *shortest_bridge_vertex_link = 0;
              List<V2<i16>>::Link *shortest_hole_vertex_link = 0;

              V2<i16> prev_hole_vertex = hole->vertices.last()->data;
              V2<i16> hole_vertex = hole->vertices.first()->data;
              for (auto *hole_vertex_link = hole->vertices.first();
                   hole_vertex_link != hole->vertices.sentinel();
                   hole_vertex_link = hole_vertex_link->next)
              {
                V2<i16> next_hole_vertex;
                {
                  List<V2<i16>>::Link *next_hole_vertex_link = hole_vertex_link->next;
                  if (next_hole_vertex_link == hole->vertices.sentinel()) next_hole_vertex_link = next_hole_vertex_link->next;
                  next_hole_vertex = next_hole_vertex_link->data;
                }

                Polygon *bridge_polygon = polygon;
                auto *next_bridge_polygon_link = hole_link->next;
                while (true)
                {
                  V2<i16> prev_bridge_vertex = bridge_polygon->vertices.last()->data;
                  V2<i16> current_bridge_vertex = bridge_polygon->vertices.first()->data;
                  for (auto *bridge_vertex_link = bridge_polygon->vertices.first();
                       bridge_vertex_link != bridge_polygon->vertices.sentinel();
                       bridge_vertex_link = bridge_vertex_link->next)
                  {
                    V2<i16> next_bridge_vertex;
                    {
                      List<V2<i16>>::Link *next_bridge_vertex_link = bridge_vertex_link->next;
                      if (next_bridge_vertex_link == polygon->vertices.sentinel()) next_bridge_vertex_link = next_bridge_vertex_link->next;
                      next_bridge_vertex = next_bridge_vertex_link->data;
                    }

                    BREAKABLE_START
                    {
                      V2<i32> bridge = v2<i32>(current_bridge_vertex - hole_vertex);
                      i32 bridge_length_squared = dot(bridge, bridge);
                      if (shortest_bridge_length_squared <= bridge_length_squared) break;

                      // If the bridge isn't inside the polygon, bail
                      {
                        V2<i32> b_h = -bridge;

                        V2<i32> p_b = v2<i32>(current_bridge_vertex - prev_bridge_vertex);
                        V2<i32> p_b_perp_inward = v2(-p_b.y, p_b.x);
                        bool hole_vertex_outside_p_b = dot(p_b_perp_inward, b_h) <= 0;

                        V2<i32> b_n = v2<i32>(next_bridge_vertex - current_bridge_vertex);
                        i32 next_bridge_vertex_dist_inside_p_b = dot(p_b_perp_inward, b_n);

                        if (next_bridge_vertex_dist_inside_p_b == 0)
                        {
                          if (hole_vertex_outside_p_b) break;
                        }
                        else
                        {
                          V2<i32> b_n_perp_inward = v2(-b_n.y, b_n.x);
                          bool hole_vertex_outside_b_n = dot(b_n_perp_inward, b_h) <= 0;

                          if (0 < next_bridge_vertex_dist_inside_p_b)
                          {
                            // Acute angle
                            if (hole_vertex_outside_p_b || hole_vertex_outside_b_n) break;
                          }
                          else // i.e. (next_bridge_vertex_dist_inside_p_b < 0)
                          {
                            // Obtuse angle
                            if (hole_vertex_outside_p_b && hole_vertex_outside_b_n) break;
                          }
                        }
                      }

                      // If the bridge isn't outside the hole, bail
                      {
                        V2<i32> h_b = bridge;

                        V2<i32> p_h = v2<i32>(hole_vertex - prev_hole_vertex);
                        V2<i32> p_h_perp_outward = v2(-p_h.y, p_h.x);
                        i32 bridge_vertex_inside_p_h = dot(p_h_perp_outward, h_b) <= 0;

                        V2<i32> h_n = v2<i32>(next_hole_vertex - hole_vertex);
                        i32 next_hole_vertex_dist_outside_p_h = dot(p_h_perp_outward, h_n);

                        if (next_hole_vertex_dist_outside_p_h == 0)
                        {
                          if (bridge_vertex_inside_p_h) break;
                        }
                        else
                        {
                          V2<i32> h_n_perp_outward = v2(-h_n.y, h_n.x);
                          i32 hole_vertex_inside_h_n = dot(h_n_perp_outward, h_b) <= 0;

                          if (0 < next_hole_vertex_dist_outside_p_h)
                          {
                            // Acute angle
                            if (bridge_vertex_inside_p_h || hole_vertex_inside_h_n) break;
                          }
                          else // i.e. (next_hole_vertex_dist_outside_p_h < 0)
                          {
                            // Obtuse angle
                            if (bridge_vertex_inside_p_h && hole_vertex_inside_h_n) break;
                          }
                        }
                      }

                      V2<i32> bridge_perp = v2<i32>(bridge.y, -bridge.x);

                      bool found_intersection = false;

                      Polygon *intersect_polygon = polygon;
                      auto *next_intersect_polygon_link = holes->first();
                      while (true)
                      {
                        List<V2<i16>>::Link *prev_intersect_vertex_link = intersect_polygon->vertices.last();
                        i32 prev_from_bridge_dot;
                        {
                          V2<i32> prev_from_hole_vertex = v2<i32>(prev_intersect_vertex_link->data - hole_vertex);
                          prev_from_bridge_dot = dot(bridge_perp, prev_from_hole_vertex);
                        }

                        for (List<V2<i16>>::Link *current_intersect_vertex_link = intersect_polygon->vertices.first();
                             current_intersect_vertex_link != intersect_polygon->vertices.sentinel();
                             current_intersect_vertex_link = current_intersect_vertex_link->next)
                        {
                          V2<i16> prev = prev_intersect_vertex_link->data;
                          V2<i16> current = current_intersect_vertex_link->data;
                          i32 current_from_bridge_dot;
                          {
                            V2<i32> current_from_hole_vertex = v2<i32>(current - hole_vertex);
                            current_from_bridge_dot = dot(bridge_perp, current_from_hole_vertex);
                          }

                          if (hole_vertex_link != prev_intersect_vertex_link &&
                              hole_vertex_link != current_intersect_vertex_link)
                          {
                            // if prev is on the bridge line, bail out!
                            if (!prev_from_bridge_dot)
                            {
                              V2<i32> hole_from_prev = v2<i32>(hole_vertex - prev);
                              V2<i32> bridge_from_prev = v2<i32>(current_bridge_vertex - prev);

                              if (dot(hole_from_prev, bridge_from_prev) < 0)
                              {
                                found_intersection = true;
                                break;
                              }
                            }

                            // if current is on the bridge line, bail out!
                            if (!current_from_bridge_dot)
                            {
                              V2<i32> hole_from_current = v2<i32>(hole_vertex - current);
                              V2<i32> bridge_from_current = v2<i32>(current_bridge_vertex - current);

                              if (dot(hole_from_current, bridge_from_current) < 0)
                              {
                                found_intersection = true;
                                break;
                              }
                            }

                            if ( (   prev_from_bridge_dot < 0 && 0 < current_from_bridge_dot) ||
                                 (current_from_bridge_dot < 0 && 0 <    prev_from_bridge_dot) )
                            {
                              V2<i16> hole_from_prev = hole_vertex - prev;
                              V2<i16> bridge_from_prev = current_bridge_vertex - prev;

                              V2<i32> hole_from_prev_perp = v2<i32>(hole_from_prev.y, -hole_from_prev.x);
                              V2<i32> bridge_from_prev_perp = v2<i32>(bridge_from_prev.y, -bridge_from_prev.x);

                              V2<i32> current_from_prev = v2<i32>(current - prev);

                              i32 current_from_prev_to_hole_line_dot = dot(hole_from_prev_perp, current_from_prev);
                              i32 current_from_prev_to_bridge_line_dot = dot(bridge_from_prev_perp, current_from_prev);

                              if ( (  current_from_prev_to_hole_line_dot < 0 && 0 < current_from_prev_to_bridge_line_dot) ||
                                   (current_from_prev_to_bridge_line_dot < 0 && 0 <   current_from_prev_to_hole_line_dot) )
                              {
                                found_intersection = true;
                                break;
                              }
                            }
                          }

                          prev_intersect_vertex_link = current_intersect_vertex_link;
                          prev_from_bridge_dot = current_from_bridge_dot;
                        }

                        if (found_intersection || next_intersect_polygon_link == holes->sentinel()) break;
                        intersect_polygon = &next_intersect_polygon_link->data;
                        next_intersect_polygon_link = next_intersect_polygon_link->next;
                      }

                      if (!found_intersection)
                      {
                        shortest_bridge_length_squared = bridge_length_squared;
                        shortest_bridge_vertex_link = bridge_vertex_link;
                        shortest_hole_vertex_link = hole_vertex_link;
                      }
                    }
                    BREAKABLE_END

                    prev_bridge_vertex = current_bridge_vertex;
                    current_bridge_vertex = next_bridge_vertex;
                  }

                  if (next_bridge_polygon_link == holes->sentinel()) break;
                  bridge_polygon = &next_bridge_polygon_link->data;
                  next_bridge_polygon_link = next_bridge_polygon_link->next;
                }

                prev_hole_vertex = hole_vertex;
                hole_vertex = next_hole_vertex;
              }

              assert(shortest_bridge_length_squared < INT32_MAX);

              {
                List<V2<i16>>::Link *bridge_vertex_revisited_link =
                  free_vertex_links.getRecycledLink(&per_dir_alloc);
                List<V2<i16>>::Link *hole_vertex_revisited_link =
                  free_vertex_links.getRecycledLink(&per_dir_alloc);

                bridge_vertex_revisited_link->data = shortest_bridge_vertex_link->data;
                hole_vertex_revisited_link->data = shortest_hole_vertex_link->data;

                shortest_bridge_vertex_link->append(bridge_vertex_revisited_link);
                shortest_bridge_vertex_link->append(hole_vertex_revisited_link);
              }

              if (hole->vertices.first() != shortest_hole_vertex_link)
              {
                List<V2<i16>>::Link *pre_bridge_hole_vertex_link = shortest_hole_vertex_link->prev;
                List<V2<i16>>::Link *hole_start_vertex_link = hole->vertices.first();

                shortest_hole_vertex_link->prev = hole->vertices.sentinel();
                hole->vertices.sentinel()->next = shortest_hole_vertex_link;

                shortest_bridge_vertex_link->append(List<V2<i16>>::Chain{hole_start_vertex_link, pre_bridge_hole_vertex_link});
              }

              if (!hole->vertices.isEmpty())
              {
                List<V2<i16>>::Link *post_bridge_hole_vertex_link = hole->vertices.first();
                List<V2<i16>>::Link *hole_end_vertex_link = hole->vertices.last();

                hole->vertices.init();

                shortest_bridge_vertex_link->append(List<V2<i16>>::Chain{post_bridge_hole_vertex_link, hole_end_vertex_link});
              }

              hole_link->remove(&free_polygon_links);

              for (auto *vertex_link = polygon->vertices.first();
                   vertex_link != polygon->vertices.sentinel();
                   vertex_link = vertex_link->next)
              {
                V2<i16> vertex = vertex_link->data;
              }
            }

            free_polygon_holes_links.append(polygon_holes_link);
          }

          // We now have a complete list of the polygons (with holes spliced
          // in) for this slice.

          if (vertex_map->occupancy)
          {
            vertex_map->clear();
          }

          while (!polygons.isEmpty())
          {
            PageAllocator per_poly_alloc = per_dir_alloc;

            auto *polygon_link = polygons.popFirst();
            SCOPE_EXIT(free_polygon_links.append(polygon_link));

            List<V2<i16>> *vertices = &polygon_link->data.vertices;

            u8 color_idx = polygon_link->data.color;
            V3<f32> color_rgb;
            {
              V4<u8> palette_color = palette->getColour(color_idx);

              color_rgb = v3<f32>(palette_color.rgb) / (f32) UINT8_MAX;
            }

            struct Corner
            {
              u32 earWidthSquared;
              List<V2<i16>>::Link *prevVertexLink;
              List<V2<i16>>::Link *cornerVertexLink;
              List<V2<i16>>::Link *nextVertexLink;
            };

            List<Corner> corners;
            List<Corner> free_corner_links;

            auto get_corner_link_if_convex = [&](List<V2<i16>>::Link *vertex_link)
            {
              List<Corner>::Link *result;

              auto *prev_link = vertex_link->prev;
              if (prev_link == vertices->sentinel()) prev_link = vertices->last();

              auto *next_link = vertex_link->next;
              if (next_link == vertices->sentinel()) next_link = vertices->first();

              V2<i32> p_c = v2<i32>(vertex_link->data - prev_link->data);
              V2<i32> n_p = v2<i32>(prev_link->data - next_link->data);
              V2<i32> n_p_perp_inward = v2(-n_p.y, n_p.x);
              i32 current_dist_outside_polygon = dot(n_p_perp_inward, p_c);

              if (current_dist_outside_polygon <= 0)
              {
                result = 0;
              }
              else
              {
                V2<i32> p_n = v2<i32>(next_link->data - prev_link->data);

                result = free_corner_links.getRecycledLink(&per_poly_alloc);
                result->data.earWidthSquared  = dot(p_n, p_n);
                result->data.prevVertexLink   = prev_link;
                result->data.cornerVertexLink = vertex_link;
                result->data.nextVertexLink   = next_link;
              }

              return result;
            };

            for (auto *vertex_link = vertices->first();
                 vertex_link != vertices->sentinel();
                 vertex_link = vertex_link->next)
            {
              auto *corner_link = get_corner_link_if_convex(vertex_link);
              if (corner_link) corners.append(corner_link);
            }

            corners.sort([](Corner *a, Corner *b) { return a->earWidthSquared <= b->earWidthSquared; });

            while (!corners.isEmpty())
            {
              auto *corner_link = corners.popFirst();

              SCOPE_EXIT(free_corner_links.append(corner_link));
              V2<i16> pv = corner_link->data.prevVertexLink->data;
              V2<i16> cv = corner_link->data.cornerVertexLink->data;
              V2<i16> nv = corner_link->data.nextVertexLink->data;

              V2<i32> p_c = v2<i32>(cv - pv);
              V2<i32> p_c_perp_inward = v2(-p_c.y, p_c.x);

              V2<i32> c_n = v2<i32>(nv - cv);
              V2<i32> c_n_perp_inward = v2(-c_n.y, c_n.x);

              V2<i32> n_p = v2<i32>(pv - nv);
              V2<i32> n_p_perp_inward = v2(-n_p.y, n_p.x);

              bool found_vertex_inside = false;

              for (auto *vertex_link = vertices->first();
                   vertex_link != vertices->sentinel();
                   vertex_link = vertex_link->next)
              {
                V2<i16> vertex = vertex_link->data;

                if (vertex == pv ||
                    vertex == cv ||
                    vertex == nv)
                {
                  continue;
                }

                i32 vertex_dist_inside_p_c = dot(p_c_perp_inward, v2<i32>(vertex - pv));
                if (vertex_dist_inside_p_c < 0) continue;

                i32 vertex_dist_inside_c_v = dot(c_n_perp_inward, v2<i32>(vertex - cv));
                if (vertex_dist_inside_c_v < 0) continue;

                i32 vertex_dist_inside_v_n = dot(n_p_perp_inward, v2<i32>(vertex - nv));
                if (vertex_dist_inside_v_n < 0) continue;

                assert(vertex_dist_inside_v_n <= 0 || (vertex_dist_inside_p_c != 0 && vertex_dist_inside_c_v != 0));

                found_vertex_inside = true;
                break;
              }

              if (!found_vertex_inside)
              {
                // Add new triangle to model:

                auto get_vert_idx = [&](V2<i16> pos)
                {
                  bool new_vertex;
                  u32 *idx = vertex_map->putPtr(TmpVertex { v2<u8>(pos), color_idx }, &new_vertex);
                  assert(idx);

                  if (new_vertex)
                  {
                    *idx = v_buffer_idx++;
                    tmp_v_buffer[*idx].pos = v3<f32>(base_corner + vectorToRHBasis(v3(a, pos), a_axis));
                    tmp_v_buffer[*idx].nrm = v3<f32>(Direction3d::VECTOR[a_axis]);
                    tmp_v_buffer[*idx].clr = color_rgb;
                  }

                  return *idx;
                };

                tmp_i_buffer[i_buffer_idx++] = get_vert_idx(pv);
                tmp_i_buffer[i_buffer_idx++] = get_vert_idx(cv);
                tmp_i_buffer[i_buffer_idx++] = get_vert_idx(nv);


                // Update vertex list:

                auto *corner_vertex_link = corner_link->data.cornerVertexLink;
                corner_vertex_link->remove();
                SCOPE_EXIT(free_vertex_links.append(corner_vertex_link));
                auto *prev_vertex_link = corner_link->data.prevVertexLink;
                auto *next_vertex_link = corner_link->data.nextVertexLink;


                // Update corner list:
                // - Insert the preceding and following corners in their proper place
                // - Remove any corners referencing our eliminated corner vertex

                List<Corner>::Link *new_links[2] = { 0, 0 };
                {
                  auto *preceding_corner_link = get_corner_link_if_convex(prev_vertex_link);
                  auto *following_corner_link = get_corner_link_if_convex(next_vertex_link);

                  if (preceding_corner_link)
                  {
                    if (following_corner_link)
                    {
                      if (preceding_corner_link->data.earWidthSquared <=
                          following_corner_link->data.earWidthSquared)
                      {
                        new_links[0] = preceding_corner_link;
                        new_links[1] = following_corner_link;
                      }
                      else
                      {
                        new_links[0] = following_corner_link;
                        new_links[1] = preceding_corner_link;
                      }
                    }
                    else
                    {
                      new_links[0] = preceding_corner_link;
                    }
                  }
                  else if (following_corner_link)
                  {
                    new_links[0] = following_corner_link;
                  }
                }



                bool yet_to_find_old_preceding = true;
                bool yet_to_find_old_following = true;

                for (auto *link = corners.first();
                     ( new_links[0] || new_links[1] ||
                       yet_to_find_old_preceding || yet_to_find_old_following )
                     && link != corners.sentinel();
                     link = link->next)
                {
                  assert(link->data.cornerVertexLink != corner_vertex_link);

                  {
                    bool remove = false;

                    if ( ( yet_to_find_old_preceding &&
                           link->data.prevVertexLink == corner_vertex_link ) )
                    {
                      remove = true;
                      yet_to_find_old_preceding = false;
                    }
                    else if ( ( yet_to_find_old_following &&
                                link->data.nextVertexLink == corner_vertex_link ) )
                    {
                      remove = true;
                      yet_to_find_old_following = false;
                    }

                    if (remove)
                    {
                      auto *to_remove = link;
                      link = link->prev;
                      to_remove->remove();
                      continue;
                    }
                  }

                  assert(link->data.cornerVertexLink != prev_vertex_link &&
                         link->data.cornerVertexLink != corner_vertex_link &&
                         link->data.cornerVertexLink != next_vertex_link);

                  if (new_links[0] &&
                      new_links[0]->data.earWidthSquared <= link->data.earWidthSquared)
                  {
                    link->prepend(new_links[0]);
                    new_links[0] = 0;
                  }

                  if (!new_links[0] && new_links[1] &&
                      new_links[1]->data.earWidthSquared <= link->data.earWidthSquared)
                  {
                    link->prepend(new_links[1]);
                    new_links[1] = 0;
                  }
                }

                if (new_links[0]) corners.append(new_links[0]);
                if (new_links[1]) corners.append(new_links[1]);
              }
            }

            assert(vertices->count() == 2);
          }
        }
      }

      Mesh *result;
      {
        u64 vertices_size = v_buffer_idx * sizeof(Mesh::Vertex);
        u64 indices_size  = i_buffer_idx * sizeof(u32);
        u8 *result_memory = allocUninitArray<u8>(heap_alloc,
                                                 sizeof(Mesh) +
                                                 vertices_size + indices_size);
        result = (Mesh *) result_memory;
        result_memory += sizeof(Mesh);
        result->vertices = (Mesh::Vertex *) result_memory;
        result_memory += vertices_size;
        result->indices = (u32 *) result_memory;
        result->size = v3<f32>(grid->size);
      }

      result->vertexCount = v_buffer_idx;
      memcpy(result->vertices, tmp_v_buffer, result->vertexCount * sizeof(Mesh::Vertex));

      result->indexCount = i_buffer_idx;
      memcpy(result->indices, tmp_i_buffer, result->indexCount * sizeof(u32));

      return result;
    }
  }
}
