#ifdef magicaVoxel_File_hpp
#error Multiple inclusion
#endif
#define magicaVoxel_File_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef magicaVoxel_Palette_hpp
#error "Please include magicaVoxel_Palette.hpp before this file"
#endif



namespace magicaVoxel
{
  struct File
  {
    static bool verifyMagicNumber(u32 magic_number, char *expected_string);

    // Define file-layout structs with no padding
    #pragma pack(push, 1)
    struct Voxel
    {
      V3<u8> pos;
      u8     i;
    };

    struct ChunkHeader
    {
      u32 magicNumber;
      u32 contentSize;
      u32 childrenSize;
    };

    struct VoxelChunk
    {
      u32   count;
      Voxel voxels[];

      NO_COPY_OR_MOVE(VoxelChunk)
    };

    union ChunkContent
    {
      V3<u32>    size;
      VoxelChunk voxel;
      Palette    palette;
    };

    struct Chunk
    {
      ChunkHeader  header;
      ChunkContent content;
    };

    struct Header
    {
      u32 magicNumber;
      u32 version;
    };
    #pragma pack(pop)

    struct ChunkReference
    {
      Chunk *chunk;
      u32    parent;
      u64    childChunksEnd;
    };

    struct ChunkYard
    {
      ChunkReference refs[32];
      u32            count;

      void initFromFileData(u8 *data, u64 size);
    };

    Palette *palette;
    Voxel   *voxels;
    u32      voxelCount;
    V3<u8>   gridSize;

    bool initUsingFileData(u8 *data, u64 size);
  };
}
