#ifdef magicaVoxel_loadMesh_hpp
#error Multiple inclusion
#endif
#define magicaVoxel_loadMesh_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef PageAllocator_hpp
#error "Please include PageAllocator.hpp before this file"
#endif

#ifndef magicaVoxel_Palette_hpp
#error "Please include magicaVoxel_Palette.hpp before this file"
#endif

#ifndef magicaVoxel_VoxelGrid_hpp
#error "Please include magicaVoxel_VoxelGrid.hpp before this file"
#endif

#ifndef magicaVoxel_Mesh_hpp
#error "Please include magicaVoxel_Mesh.hpp before this file"
#endif


namespace magicaVoxel
{
  namespace loadMesh
  {
    Mesh *from(HeapAllocator *heap_alloc, VoxelGrid *grid, Palette *palette);
    Mesh *from(HeapAllocator *heap_alloc, PageAllocator tmp_alloc, VoxelGrid *grid, Palette *palette);
  }
}
