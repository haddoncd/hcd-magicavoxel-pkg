#ifdef magicaVoxel_Mesh_hpp
#error Multiple inclusion
#endif
#define magicaVoxel_Mesh_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef vector_hpp
#error "Please include vector.hpp before this file"
#endif



namespace magicaVoxel
{
  struct Mesh
  {
    struct Vertex
    {
      V3<f32> pos;
      V3<f32> nrm;
      V3<f32> clr;
    };

    u32      vertexCount;
    u32      indexCount;
    Vertex  *vertices;
    u32     *indices;
    V3<f32>  size;
  };
}
