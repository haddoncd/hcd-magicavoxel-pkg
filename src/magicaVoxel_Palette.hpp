#ifdef magicaVoxel_Palette_hpp
#error Multiple inclusion
#endif
#define magicaVoxel_Palette_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef vector_hpp
#error "Please include vector.hpp before this file"
#endif



namespace magicaVoxel
{
  #pragma pack(push, 1)
  struct Palette
  {
    // FIXME: Should the colours in the palette be assumed to be linear, gamma
    //        corrected, SRGB, or what?
    V4<u8> colours[255];
    V4<u8> unused;

    V4<u8> *getColourPtr(u8 index);

    V4<u8> getColour(u8 index);

    static Palette DEFAULT;
  };
  #pragma pack(pop)
}
