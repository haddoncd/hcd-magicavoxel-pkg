#ifdef magicaVoxel_VoxelGrid_hpp
#error Multiple inclusion
#endif
#define magicaVoxel_VoxelGrid_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef magicaVoxel_File_hpp
#error "Please include magicaVoxel_File.hpp before this file"
#endif



namespace magicaVoxel
{
  struct VoxelGrid
  {
    u32    voxelCount;
    V3<u8> size;
    u8     voxels[];

    NO_COPY_OR_MOVE(VoxelGrid)

    static size_t sizeWith(V3<u8> size);

    u8 *getVoxelPtr(V3<i16> pos);

    u8 getVoxel(V3<i16> pos);

    template <typename A>
    static VoxelGrid *allocAndInitFromFile(A allocator, File file)
    {
      VoxelGrid *result = allocSized<VoxelGrid>(allocator, VoxelGrid::sizeWith(file.gridSize));

      result->size.x = file.gridSize.x;
      result->size.y = file.gridSize.y;
      result->size.z = file.gridSize.z;

      for (u32 i = 0; i < file.voxelCount; ++i)
      {
        File::Voxel *file_voxel = &file.voxels[i];

        V3<i16> pos = v3<i16>(file_voxel->pos.x,
                              file_voxel->pos.y,
                              file_voxel->pos.z);

        u8 *grid_voxel = result->getVoxelPtr(pos);

        if (!grid_voxel)
        {
          // Discarding OOB voxel
          continue;
        }

        *grid_voxel = file_voxel->i;
        ++result->voxelCount;
      }

      return result;
    }
  };
}
